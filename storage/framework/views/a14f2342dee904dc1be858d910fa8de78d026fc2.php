<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Document</title>
</head>

<body>
    <form method="POST" action="">
    <!-- <input type='hidden' name='email' value='hp echo $email;?>' /> -->
        <div class="container">
            <div class="page-header">
                <h1 class="well">Change Password For User</h1>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="form-group row">
                    <label for="inputpassword" class="col-sm-2 col-form-label">Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" name="password" id="inputpassword"
                            placeholder="Enter your password" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputpassword1" class="col-sm-2 col-form-label">Confirm Password</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="con_password" id="inputpassword1" 
                        placeholder="Enter your Confirm Password" required>
                    </div>
                </div>

                <div class="col-sm-12" style="text-align:center">
                    <button class="btn btn-primary btn-md" style="background-color: #3b5998" type="submit" 
                    role=" button">Submit</button>
                </div>
            </div>
        </div>
    </form>
</body>

</html><?php /**PATH /home/sumbul/recruit-management-system/resources/views//v1/email_verify.blade.php ENDPATH**/ ?>