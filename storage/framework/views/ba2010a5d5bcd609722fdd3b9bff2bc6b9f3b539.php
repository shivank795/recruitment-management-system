<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <style>
        .container {
            width: 500px;
        }

    </style>
    <title>Login Page</title>
</head>

<body>
    <form method="POST" action="/login">
        <div class="container">
            <div class="page-header">
                <h1 class="well" style="text-align:center">Login-Register</h1>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="form-group row">
                    <label for="inputemail" class="col-sm-2 col-form-label">Email </label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" name="email" id="inputemail"
                            placeholder="Enter your Email">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputpassword" class="col-sm-2 col-form-label">Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" name="password" id="inputpassword"
                            placeholder="Enter your password">
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-8">
                            <input class="form-check-input" type="checkbox" value="" id="loginCheck" checked />
                            <label class="form-check-label" for="loginCheck"> Remember me </label>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <a href="../users/forgot.php">Forgot password?</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <button class="btn btn-primary btn-lg" style="background-color: #3b5998" type="submit"
                            role=" button">Login</button>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <button class="btn btn-primary btn-lg" style="background-color: #3b5998"
                            role="button">Register</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
<?php /**PATH /home/sumbul/recruit-management-system/resources/views/v1/login.blade.php ENDPATH**/ ?>