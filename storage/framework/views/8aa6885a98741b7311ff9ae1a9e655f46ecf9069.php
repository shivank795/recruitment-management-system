<nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            <img alt="image" class="rounded-circle" src="<?php echo e(asset('img/a6.jpg')); ?>" width="100" height="80"/>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="block m-t-xs font-bold"></span>
                                <span class="text-muted text-xs block">Sumbul Khan<b class="caret"></b></span>
                            </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a class="dropdown-item" href="profile.html">Profile</a></li>
                                <li><a class="dropdown-item" href="contacts.html">Contacts</a></li>
                                <li><a class="dropdown-item" href="mailbox.html">Mailbox</a></li>
                                <li class="dropdown-divider"></li>
                                <li><a class="dropdown-item" href="">Logout</a></li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            IN+
                        </div>
                    </li>
                    <!-- <li>
                        <a href=""><i class="fa fa-graduation-cap"></i> <span class="nav-label">Students</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li class="active"><a href="<?php echo e(url('/students/create')); ?>">Create</a></li>
                            <li><a href="<?php echo e(url('/students/list')); ?>">List</a></li>
                        </ul>
                    </li> -->
                    <li>
                        <a href=""><i class="fa fa-users" aria-hidden="true"></i> <span class="nav-label">Users</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li><a href="<?php echo e(url('/v1/users/create')); ?>">Create</a></li>
                            <li><a href="<?php echo e(url('/v1/users/list')); ?>">List</a></li>
                        </ul>
                    </li>
                    <!-- <li>
                        <a href=""><i class="fa fa-building-o" aria-hidden="true"></i> <span class="nav-label">Teachers</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li class="active"><a href="<?php echo e(url('/teacher/create')); ?>">Create</a></li>
                            <li><a href="<?php echo e(url('/teacher/list')); ?>">List</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href=""><i class="fa fa-building-o" aria-hidden="true"></i> <span class="nav-label">Subjects</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li class="active"><a href="<?php echo e(url('/subject/create')); ?>">Create</a></li>
                            <li><a href="<?php echo e(url('/subject/list')); ?>">List</a></li>
                        </ul>
                    </li> -->
                </ul>
            </div>
        </nav><?php /**PATH /home/sumbul/recruit-management-system/resources/views/v1/layouts/sidebar.blade.php ENDPATH**/ ?>